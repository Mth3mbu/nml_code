﻿using System;
using System.Linq;
using Nml.Improve.Me.Dependencies;

namespace Nml.Improve.Me
{
    public class PdfApplicationDocumentGenerator : IApplicationDocumentGenerator
    {
        private readonly IDataContext _dataContext;
        private IPathProvider _templatePathProvider;
        public IViewGenerator _viewGenerator;
        internal readonly IConfiguration _configuration;
        private readonly ILogger<PdfApplicationDocumentGenerator> _logger;
        private readonly IPdfGenerator _pdfGenerator;
        private const string TemplatePathProviderException = "templatePathProvider";
        public PdfApplicationDocumentGenerator(
            IDataContext dataContext,
            IPathProvider templatePathProvider,
            IViewGenerator viewGenerator,
            IConfiguration configuration,
            IPdfGenerator pdfGenerator,
            ILogger<PdfApplicationDocumentGenerator> logger)
        {
            if (dataContext != null)
                throw new ArgumentNullException(nameof(dataContext));

            _dataContext = dataContext;
            _templatePathProvider = templatePathProvider ?? throw new ArgumentNullException(TemplatePathProviderException);
            _viewGenerator = viewGenerator;
            _configuration = configuration;
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _pdfGenerator = pdfGenerator;
        }

        public byte[] Generate(Guid applicationId, string baseUri)
        {
            Application application = _dataContext.Applications.Single(app => app.Id == applicationId);
            if (application == null)
            {
                var message = $"No application found for id '{applicationId}'";
                Log(message);

                return null;
            }

            var view = GenerateView(application, baseUri);

            if (view == null)
            {
                var message = $"The application is in state '{application.State}' and no valid document can be generated for it.";
                Log(message);

                return null;
            }

            var pdfOptions = new PdfOptions
            {
                PageNumbers = PageNumbers.Numeric,
                HeaderOptions = new HeaderOptions
                {
                    HeaderRepeat = HeaderRepeat.FirstPageOnly,
                    HeaderHtml = PdfConstants.Header
                }
            };

            var pdf = _pdfGenerator.GenerateFromHtml(view, pdfOptions);

            return pdf.ToBytes();
        }

        private static string GenerateReviewMessage(string reviewReason)
        {
            var bank = "bank";
            var address = "address";
            var headMessage = "Your application has been placed in review";
            var ficaMessage = " pending outstanding address verification for FICA purposes.";
            var bankVerificationMessage = " pending outstanding bank account verification.";
            var supportMessage = " because of suspicious account behaviour. Please contact support ASAP.";

            return headMessage +
                                reviewReason switch
                                {
                                    { } reason when reason.Contains(address) =>
                                        ficaMessage,
                                    { } reason when reason.Contains(bank) =>
                                        bankVerificationMessage,
                                    _ =>
                                        supportMessage
                                };
        }

        private string GeneratePath<T>(string baseUri, string path, T application)
        {
            return _viewGenerator.GenerateFromPath(string.Format("{0}{1}", baseUri, path), application);
        }

        private string GetTemplatePath(string status)
        {
            return _templatePathProvider.Get(status);
        }

        private void Log(string message)
        {
            _logger.LogWarning(message);
        }

        private string GenerateView(Application application, string baseUri)
        {
            var delimiter = "/";
            if (baseUri.EndsWith(delimiter))
                baseUri = baseUri.Substring(baseUri.Length - 1);

            if (application.State == ApplicationState.Pending)
            {
                var path = GetTemplatePath(AppStatus.PendingApplication);
                PendingApplicationViewModel pendingApplication = new PendingApplicationViewModel
                {
                    ReferenceNumber = application.ReferenceNumber,
                    State = application.State.ToDescription(),
                    FullName = $"{application.Person.FirstName} {application.Person.Surname}",
                    AppliedOn = application.Date,
                    SupportEmail = _configuration.SupportEmail,
                    Signature = _configuration.Signature
                };

                return GeneratePath(baseUri, path, pendingApplication);
            }

            if (application.State == ApplicationState.Activated)
            {
                var path = GetTemplatePath(AppStatus.ActivatedApplication);
                ActivatedApplicationViewModel ActivatedApplication = new ActivatedApplicationViewModel
                {
                    ReferenceNumber = application.ReferenceNumber,
                    State = application.State.ToDescription(),
                    FullName = $"{application.Person.FirstName} {application.Person.Surname}",
                    LegalEntity = application.IsLegalEntity ? application.LegalEntity : null,
                    PortfolioFunds = application.Products.SelectMany(p => p.Funds),
                    PortfolioTotalAmount = application.Products.SelectMany(p => p.Funds)
                                                    .Select(f => (f.Amount - f.Fees) * _configuration.TaxRate)
                                                    .Sum(),
                    AppliedOn = application.Date,
                    SupportEmail = _configuration.SupportEmail,
                    Signature = _configuration.Signature
                };

                return GeneratePath(baseUri, path, ActivatedApplication);
            }

            if (application.State == ApplicationState.InReview)
            {
                var templatePath = GetTemplatePath(AppStatus.InReviewApplication);
                var inReviewMessage = GenerateReviewMessage(application.CurrentReview.Reason);
                var inReviewApplicationViewModel = new InReviewApplicationViewModel();
                inReviewApplicationViewModel.ReferenceNumber = application.ReferenceNumber;
                inReviewApplicationViewModel.State = application.State.ToDescription();
                inReviewApplicationViewModel.FullName = string.Format(
                    "{0} {1}",
                    application.Person.FirstName,
                    application.Person.Surname);
                inReviewApplicationViewModel.LegalEntity =
                    application.IsLegalEntity ? application.LegalEntity : null;
                inReviewApplicationViewModel.PortfolioFunds = application.Products.SelectMany(p => p.Funds);
                inReviewApplicationViewModel.PortfolioTotalAmount = application.Products.SelectMany(p => p.Funds)
                    .Select(f => (f.Amount - f.Fees) * _configuration.TaxRate)
                    .Sum();
                inReviewApplicationViewModel.InReviewMessage = inReviewMessage;
                inReviewApplicationViewModel.InReviewInformation = application.CurrentReview;
                inReviewApplicationViewModel.AppliedOn = application.Date;
                inReviewApplicationViewModel.SupportEmail = _configuration.SupportEmail;
                inReviewApplicationViewModel.Signature = _configuration.Signature;

                return GeneratePath(baseUri, templatePath, inReviewApplicationViewModel);
            }

            return null;
        }
    }
}
