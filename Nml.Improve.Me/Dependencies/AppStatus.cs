﻿namespace Nml.Improve.Me.Dependencies
{
    public static class AppStatus
    {
        public const string PendingApplication = "PendingApplication";
        public const string ActivatedApplication = "ActivatedApplication";
        public const string InReviewApplication = "InReviewApplication";
    }
}
