﻿using System;
using System.Threading.Tasks;
using Nml.Refactor.Me.Dependencies;
using Nml.Refactor.Me.MessageBuilders;

namespace Nml.Refactor.Me.Notifiers
{
    public class SmsNotifier : INotifier
    {
        private readonly INotificationHandler _notificationHandler;
        private readonly IStringMessageBuilder _messageBuilder;
        private readonly IOptions _options;
        private readonly ILogger _logger = LogManager.For<SmsNotifier>();

        public SmsNotifier(IStringMessageBuilder messageBuilder, INotificationHandler notificationService, IOptions options)
        {
            _messageBuilder = messageBuilder ?? throw new ArgumentNullException(nameof(messageBuilder));
            _options = options ?? throw new ArgumentNullException(nameof(options));
            _notificationHandler = notificationService;
        }

        public async Task Notify(NotificationMessage message)
        {
            var smsClient = new SmsApiClient(_options.Sms.ApiUri, _options.Sms.ApiKey);
            var smsMessage = _messageBuilder.CreateMessage(message);

           await _notificationHandler.Execute(smsClient.SendAsync(message.To, smsMessage), _logger);
        }
    }
}
