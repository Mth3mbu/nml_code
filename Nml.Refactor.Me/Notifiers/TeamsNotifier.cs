﻿using System;
using System.Threading.Tasks;
using Nml.Refactor.Me.Dependencies;

namespace Nml.Refactor.Me.Notifiers
{
    public class TeamsNotifier : INotifier
    {
        private readonly IInstantMessageNotification _instantMessageNotification;
        private readonly IOptions _options;
        private readonly ILogger _logger = LogManager.For<TeamsNotifier>();

        public TeamsNotifier(IOptions options, IInstantMessageNotification instantMessageNotification)
        {
            _options = options ?? throw new ArgumentNullException(nameof(options));
            _instantMessageNotification = instantMessageNotification;
        }

        public async Task Notify(NotificationMessage message)
        {
            await _instantMessageNotification.SendNotification(message, _logger, _options.Teams.WebhookUri);
        }
    }
}
