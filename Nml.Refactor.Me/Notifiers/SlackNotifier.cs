﻿using System;
using System.Threading.Tasks;
using Nml.Refactor.Me.Dependencies;

namespace Nml.Refactor.Me.Notifiers
{
    public class SlackNotifier : INotifier
    {
        private readonly IInstantMessageNotification _instantMessageNotification;
        private readonly IOptions _options;
        private readonly ILogger _logger = LogManager.For<SlackNotifier>();

        public SlackNotifier(IInstantMessageNotification instantMessageNotification, IOptions options)
        {
            _options = options ?? throw new ArgumentNullException(nameof(options));
            _instantMessageNotification = instantMessageNotification;
        }

        public async Task Notify(NotificationMessage message)
        {
            await _instantMessageNotification.SendNotification(message, _logger, _options.Slack.WebhookUri);
        }
    }
}
