﻿using System;
using System.Threading.Tasks;

namespace Nml.Refactor.Me.Dependencies
{
    public class Notification : INotificationHandler
    {
        public async Task Execute(Task action, ILogger logger)
        {
            try
            {
                await action;
                logger.LogTrace($"Message sent.");
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Failed to send message. {e.Message}");
                throw;
            }
        }
    }
}
