﻿using Nml.Refactor.Me.MessageBuilders;
using Nml.Refactor.Me.Notifiers;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Nml.Refactor.Me.Dependencies
{
    public class InstantMessageNotification : IInstantMessageNotification
    {
        private readonly IWebhookMessageBuilder _messageBuilder;
        public InstantMessageNotification(IWebhookMessageBuilder messageBuilder)
        {
            _messageBuilder = messageBuilder ?? throw new ArgumentNullException(nameof(messageBuilder));
        }
        public async Task SendNotification(NotificationMessage message, ILogger logger, string uri)
        {
            var serviceEndPoint = new Uri(uri);
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Post, serviceEndPoint);
            request.Content = new StringContent(
                _messageBuilder.CreateMessage(message).ToString(),
                Encoding.UTF8,
                "application/json");
            try
            {
                var response = await client.SendAsync(request);

                logger.LogTrace($"Message sent. {response.StatusCode} -> {response.Content}");
            }
            catch (AggregateException e)
            {
                foreach (var exception in e.Flatten().InnerExceptions)
                    logger.LogError(exception, $"Failed to send message. {exception.Message}");

                throw;
            }
        }
    }
}
