﻿using System.Threading.Tasks;

namespace Nml.Refactor.Me.Dependencies
{
    public interface INotificationHandler
    {
        Task Execute(Task action, ILogger logger);
    }
}
