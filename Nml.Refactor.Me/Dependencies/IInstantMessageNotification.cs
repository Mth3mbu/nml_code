﻿using Nml.Refactor.Me.Notifiers;
using System.Threading.Tasks;

namespace Nml.Refactor.Me.Dependencies
{
    public interface IInstantMessageNotification
    {
        Task SendNotification(NotificationMessage message, ILogger logger, string uri);
    }
}
